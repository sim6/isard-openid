#!/bin/bash
set -e
## It will create keytab for mokey client

mkdir -p /etc/mokey/keytab
echo $IPA_ADMIN_PWD | kinit admin
ipa role-add 'Mokey User Manager' --desc='Mokey User management'
ipa role-add-privilege 'Mokey User Manager' --privilege='User Administrators'
ipa user-add mokeyapp --first Mokey --last App
ipa role-add-member 'Mokey User Manager' --users=mokeyapp
ipa-getkeytab -s $DOMAIN -p mokeyapp -k /etc/mokey/keytab/mokeyapp.keytab
chmod 640 /etc/mokey/keytab/mokeyapp.keytab
