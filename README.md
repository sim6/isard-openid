# IsardVDI - OpenID infrastructure

This will bring up a full OpenID auth infrastructure consisting in:

- FreeIPA: https://<domain>/ipa
- Mokey: https://<domain>
- Hydra: https://<domain>/hydra

NOTE: If you use the default example domain in main.conf.example you will need to add this domain mapping to IP in your hosts file at clients.

# Quick start

1. Edit main.conf (to suit your needs). If you set Letsencrypt vars it will ask and renew the certificate automatically.
2. Build docker.compose.yml: ```./build.sh```
3. Bring containers up: ```docker-compose up -d```

Wait till freeipa container is ready (it can take several minutes the first time):
```
docker logs freeipa --follow
```
And wait to be ready. It will log: *FreeIPA server configured.*

4. Add mokey client in freeipa: ```docker exec freeipa /bin/sh -c "scripts/mokey.sh"```
5. check that mokey is up: ```docker logs mokey --follow```. It will show: "⇨ http server started on [::]:8080"

Access your IP/DNS and login page should come up.

# Firewall

You should open 80 and 443

# Add client apps

## The easy way

[Not working! Refer to 'Do it yourself']

For most client apps the default script will be enough:

```
docker exec \
    -e APP_ID=<app name> \
    -e APP_SECRET=<app secret> \
    -e APP_CALLBACKS=<app callback url wiht https://...> \
    hydra /bin/sh -c "scripts/add_app.sh"
```

## Do it yourself
For example we will be creating a moodle app client. You need to adapt the vars to your app

APP_CALLBACK=https://<moodle-server>/auth/oidc/
APP_ID=moodle
APP_SECRET=Sup3rS3cr3t

docker-compose  exec hydra \
    hydra clients create \
    --endpoint http://hydra:4445/ \
    --id $APP_ID \
    --secret $APP_SECRET \
    --grant-types client_credentials,authorization_code,refresh_token \
    --token-endpoint-auth-method client_secret_post \
    --response-types code \
    --scope openid,offline,profile,email \
    --callbacks APP_CALLBACK

And then validate the app:

docker-compose exec hydra \
    hydra token client \
    --endpoint http://hydra:4444/ \
    --client-id APP_ID \
    --client-secret APP_SECRET
